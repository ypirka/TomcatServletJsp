package com.mycompany.project;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DataBaseConnection
 */
@WebServlet("/DataBaseConnection")
public class DataBaseConnection extends HttpServlet {
	private final String USER = "root";
	private final String PASS = "admin";
	private final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
	private final String DATABASE_URL = "jdbc:mysql://localhost:3306/users";
	private Statement statement;
	private Connection connection;

	public DataBaseConnection() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();
		try {
			Class.forName(DRIVER_CLASS);
			connection = DriverManager.
					getConnection(DATABASE_URL, USER, PASS);
			statement = connection.createStatement();
  
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String mdHash = convertToMD5(login, password);
		String sql = "Select * from users.usertable" + " where (`hash` like '" + mdHash + "');";
		String sqlUpdate = "update usertable " + 
				"set visits=%d where `hash` like '%s';";
		ResultSet resultSet;
		try {
			resultSet = statement.executeQuery(sql);
			if (resultSet.first()) {
				String hash = resultSet.getString("hash");
				long visits = resultSet.getLong("visits");
				statement.executeUpdate(sqlUpdate.format(sqlUpdate,++visits, mdHash));
				System.out.println("User visits= "+visits);
				System.out.println("User hash= " + hash);
				String nextJsp = "/Catalog.jsp";
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJsp);
				dispatcher.forward(request, response);
				return;
			}else {
				System.out.println("SendError 401");
				response.sendError(401);
				return;
			}
		} catch (SQLException e) {
			response.sendError(401);
			e.printStackTrace();
			return;
		}
	}

 	private synchronized String convertToMD5(String login, String pass) {
		String mdHash = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(md.digest(login.getBytes()));
			md.update(md.digest(pass.getBytes()));
			BigInteger bigInt = new BigInteger(1, md.digest());
			mdHash = bigInt.toString(16);
			System.out.println("mdHash: " + mdHash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mdHash;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
